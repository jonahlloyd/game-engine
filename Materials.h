#ifndef GAMEENGINE_MATERIALS_H
#define GAMEENGINE_MATERIALS_H

#include <GL/glut.h>

class Materials {
public:
    GLfloat BLACK[3] = {0, 0, 0};
    GLfloat WHITE[3] = {1, 1, 1};
    GLfloat CYAN[3] = {0, 1, 1};
    GLfloat ORANGE[3] = {1, 0.65, 0};
    GLfloat DARK_RED[3] = {0.55, 0, 0};
    GLfloat DARK_VIOLET[3] = {0.58, 0, 0.827};
};

#endif
