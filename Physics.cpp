#include "Physics.h"

Physics::Physics() {
    gravity_p = -30;
    gravity = gravity_p;
}

float Physics::KineticEnergy(float velocity, float mass) {
    return 0.5 * mass * (velocity * velocity);
}
