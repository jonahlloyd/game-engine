# Game Engine

Engine built from scratch to simulate real world effects on a bouncing ball,
the engine incorporates basic physics such as gravity, kinetic energy,
potential energy and energy loss between the two due to elasticity.
There is also the physics for wind.

## Installation

Built using Qt version 5.9.5 and qmake 3.1

In the same directory as the files run

```bash
qmake
```

Then run

```bash
make
```

To run the engine

```bash
./GameEngine
```

## Controls

Left click mouse and drag moves camera around view plane
Right click mouse and drag moves camera's view angle
Mouse scroll zooms camera in and out

Play button runs the simulation
Pause button pauses the simulation
Stop button ends the simulation
Refresh button restarts the simulation, always press refresh after changing the settings of the simulation to put them into effect
Reset button resets the world and camera
