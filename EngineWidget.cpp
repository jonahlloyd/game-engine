#include "EngineWidget.h"
#include "Plane.h"
#include "Camera.h"
#include "Materials.h"
#include "Ball.h"
#include "Wind.h"
#include "Window.h"
#include "Vector.h"

Camera camera;
Vector cameraLook;
Materials materials;
Plane plane;
QElapsedTimer GlobalTimer;
Wind wind;
Ball ball;

EngineWidget::EngineWidget(QWidget *parent) {

    // Set position for ball
    xPos = new QDoubleSpinBox(this);
    xPosLabel = new QLabel(this);
    yPos = new QDoubleSpinBox(this);
    yPosLabel = new QLabel(this);
    zPos = new QDoubleSpinBox(this);
    zPosLabel = new QLabel(this);

    // Initial Velocity
    velocity = new QSpinBox(this);
    velocityLabel = new QLabel(this);

    // Spin boxes for wind speed
    windSB = new QSpinBox(this);
    windLabel = new QLabel(this);

    // Create pause button
    pause_button = new QPushButton("Play", this);
    pause_button->setGeometry(QRect(QPoint(20, 25), QSize(60, 30)));
    // Create restart button
    restart_button = new QPushButton("Refresh", this);
    restart_button->setGeometry(QRect(QPoint(20, 95), QSize(60, 30)));
    // Create stop button
    stop_button = new QPushButton("Stop", this);
    stop_button->setGeometry(QRect(QPoint(20, 60), QSize(60, 30)));
    // Create reset button
    reset_button = new QPushButton("Reset", this);
    reset_button->setGeometry(QRect(QPoint(20, 130), QSize(60, 30)));

    // Create north button
    north_button = new QPushButton("N", this);
    north_button->setGeometry(QRect(QPoint(20, 230), QSize(30, 30)));
    north_button->setCheckable(true);
    // Create east button
    east_button = new QPushButton("E", this);
    east_button->setGeometry(QRect(QPoint(55, 230), QSize(30, 30)));
    east_button->setCheckable(true);
    // Create south button
    south_button = new QPushButton("S", this);
    south_button->setGeometry(QRect(QPoint(90, 230), QSize(30, 30)));
    south_button->setCheckable(true);
    // Create west button
    west_button = new QPushButton("W", this);
    west_button->setGeometry(QRect(QPoint(125, 230), QSize(30, 30)));
    west_button->setCheckable(true);

    CreateSpinBoxes();

    // Connect button signal to slot
    // Pause / Play
    connect(pause_button, SIGNAL(released()), this, SLOT(PlayPauseButton()));
    // Restart
    connect(restart_button, SIGNAL(released()), this, SLOT(RestartButton()));
    // Stop
    connect(stop_button, SIGNAL(released()), this, SLOT(StopButton()));
    // Reset
    connect(reset_button, SIGNAL(released()), this, SLOT(ResetButton()));

    // Wind directions
    connect(north_button, SIGNAL(released()), this, SLOT(NorthButton()));

    connect(east_button, SIGNAL(released()), this, SLOT(EastButton()));

    connect(south_button, SIGNAL(released()), this, SLOT(SouthButton()));

    connect(west_button, SIGNAL(released()), this, SLOT(WestButton()));

    // X
    connect(xPos, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [=](float i) { ballX = i; });
    // Y
    connect(yPos, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [=](float i) { ballY = i; });
    // Z
    connect(zPos, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [=](float i) { ballZ = i; });
    // Wind Speed
    connect(windSB, QOverload<int>::of(&QSpinBox::valueChanged),
            [=](float i) { windSpeed = i; });
    // Initial Velocity
    connect(velocity, QOverload<int>::of(&QSpinBox::valueChanged),
            [=](float i) { uVelocity = i; });

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateScene()));
    timer->start(0);

    // Set delta to 16.67 ms for frame rate
    dt = 1000.0 / 60;
    paused = 1;

    delta = 0;
    dTimePrev = 0;
    dTimeCurr = 0;

    ballX = 5.5;
    ballY = 5.5;
    ballZ = 5;

    windSpeed = 0;
    windDirX = 0;
    windDirZ = 0;
    uVelocity = 0;

    stopFlag = 0;

}

void EngineWidget::CreateWorld() {
    Wind windValues(windDirX, windDirZ, windSpeed);
    wind = windValues;
    plane.create(12, 12, 0);
}

void EngineWidget::DeclareObjects() {
    GlobalTimer.start();
    QElapsedTimer *pGlobalTimer = &GlobalTimer;
    int *pPaused = &paused;
    ball = Ball(plane, 3.0, 0.4, materials.DARK_VIOLET, ballY, ballX, ballZ, uVelocity, wind, pGlobalTimer, pPaused);
}

void EngineWidget::FrameRateGoverning() {
    dTimeCurr = GlobalTimer.nsecsElapsed();
    // Convert to milliseconds
    delta = (dTimeCurr - dTimePrev) * 1e-6;
    dTimePrev = GlobalTimer.nsecsElapsed();
}

void EngineWidget::CreateSpinBoxes() {

    //X
    xPosLabel->setText("Length");
    xPosLabel->setStyleSheet("font: 12pt; color: white");
    xPosLabel->setGeometry(125, 5, 100, 20);
    xPos->setGeometry(120, 24, 60, 30);
    xPos->setRange(1, 11);
    xPos->setSingleStep(0.5);
    xPos->setValue(5.5);
    xPos->show();

    //Z
    zPosLabel->setText("Width");
    zPosLabel->setStyleSheet("font: 12pt; color: white");
    zPosLabel->setGeometry(208, 5, 100, 20);
    zPos->setGeometry(200, 24, 60, 30);
    zPos->setRange(1, 11);
    zPos->setSingleStep(0.5);
    zPos->setValue(5.5);
    zPos->show();

    //Y
    yPosLabel->setText("Height");
    yPosLabel->setStyleSheet("font: 12pt; color: white");
    yPosLabel->setGeometry(285, 5, 100, 20);
    yPos->setGeometry(280, 24, 60, 30);
    yPos->setRange(1, 10);
    yPos->setSingleStep(0.5);
    yPos->setValue(5);
    yPos->show();

    // Velocity
    velocityLabel->setText("Velocity");
    velocityLabel->setStyleSheet("font: 12pt; color: white");
    velocityLabel->setGeometry(360, 5, 100, 20);
    velocity->setGeometry(360, 24, 60, 30);
    velocity->setRange(0, 50);
    velocity->setSingleStep(10);
    velocity->setValue(0);
    velocity->show();

    // Wind Speed
    windLabel->setText("Wind Speed");
    windLabel->setStyleSheet("font: 12pt; color: white");
    windLabel->setGeometry(20, 175, 100, 20);
    windSB->setGeometry(20, 195, 60, 30);
    windSB->setRange(0, 100);
    windSB->setSingleStep(10);
    windSB->setValue(0);
    windSB->show();
}

void EngineWidget::updateScene() {
    // Motion calculation for balls
    if (stopFlag == 0) {
        ball.update();
    }
    // Frame Governing computation
    FrameRateGoverning();
    // Check correct delta for set fps
    if (delta < dt) {
        while (delta < dt) {
            dTimeCurr = GlobalTimer.nsecsElapsed();
            // Convert to milliseconds
            delta = (dTimeCurr - dTimePrev) * 1e-6;
        }
        this->repaint();
    }
}

void EngineWidget::initializeGL() {

    GLfloat lightPosition[] = {4, 3, 7, 1};

    glEnable(GL_DEPTH_TEST);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, materials.WHITE);
    glLightfv(GL_LIGHT0, GL_SPECULAR, materials.WHITE);
    glMaterialfv(GL_FRONT, GL_SPECULAR, materials.WHITE);
    glMaterialf(GL_FRONT, GL_SHININESS, 30);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glClearColor(0.25, 0.25, 0.28, 0.0);
    // Add objects to scene
    CreateWorld();
    DeclareObjects();
}


void EngineWidget::resizeGL(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0);
    glMatrixMode(GL_MODELVIEW);
}

void EngineWidget::paintGL() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    // Vectors for calculating camera look at
    Vector cameraPos(camera.getX(), camera.getY(), camera.getZ());
    Vector cameraLook = cameraPos.Add(camera.cameraFront);
    // Sets position and look at for camera
    gluLookAt(camera.getX(), camera.getY(), camera.getZ(),
              cameraLook.getX(), cameraLook.getY(), cameraLook.getZ(),
              0.0, 1.0, 0.0);
    plane.draw();
    if (stopFlag == 0) {
        ball.draw();
    }
    glFlush();
}

void EngineWidget::PlayPauseButton() {
    if (pause_button->text() == "Pause") {
        paused = 1;
        pause_button->setText("Play");
        // Resize button
        pause_button->resize(60, 30);
    } else if (pause_button->text() == "Play") {
        paused = 0;
        pause_button->setText("Pause");
        // Resize button
        pause_button->resize(60, 30);
    }
}

void EngineWidget::RestartButton() {
    pause_button->setText("Play");
    paused = 1;
    // Resize button
    pause_button->resize(60, 30);
    pause_button->setVisible(true);

    stopFlag = 0;
    CreateWorld();
    DeclareObjects();
}

void EngineWidget::StopButton() {
    stopFlag = 1;
    pause_button->setVisible(false);
}

void EngineWidget::ResetButton() {
    pause_button->setText("Play");
    paused = 1;
    // Resize button
    pause_button->resize(60, 30);
    pause_button->setVisible(true);

    stopFlag = 0;
    CreateWorld();
    DeclareObjects();

    // Reset camera position
    camera.reset();
}

void EngineWidget::NorthButton() {
  north_button->setChecked(true);
  east_button->setChecked(false);
  south_button->setChecked(false);
  west_button->setChecked(false);
  wind.North();
  windDirX = wind.DirX();
  windDirZ = wind.DirZ();
}

void EngineWidget::EastButton() {
  north_button->setChecked(false);
  east_button->setChecked(true);
  south_button->setChecked(false);
  west_button->setChecked(false);
  wind.East();
  windDirX = wind.DirX();
  windDirZ = wind.DirZ();
}

void EngineWidget::SouthButton() {
  north_button->setChecked(false);
  east_button->setChecked(false);
  south_button->setChecked(true);
  west_button->setChecked(false);
  wind.South();
  windDirX = wind.DirX();
  windDirZ = wind.DirZ();
}

void EngineWidget::WestButton() {
  north_button->setChecked(false);
  east_button->setChecked(false);
  south_button->setChecked(false);
  west_button->setChecked(true);
  wind.West();
  windDirX = wind.DirX();
  windDirZ = wind.DirZ();
}


void EngineWidget::mousePressEvent(QMouseEvent *event) {
    lastPos = event->pos();
}

void EngineWidget::mouseMoveEvent(QMouseEvent *event) {
    double sensitivity = 0.0005;

    // If mouse moved and clicked the left mouse for translation
    if (event->buttons() & Qt::LeftButton) {
        double dx = event->x() - lastPos.x();
        double dy = event->y() - lastPos.y();

        // Invert direction and add sensitivity
        dx = -dx * sensitivity;
        dy = dy * sensitivity;

        camera.moveX(dx);
        camera.moveY(dy);
    }
        // If mouse moved and clicked the right mouse for look at direction
    else if (event->buttons() & Qt::RightButton) {
        double dxLook = event->x() - lastPos.x();
        double dyLook = event->y() - lastPos.y();

        dyLook = dyLook * sensitivity * 0.2;
        dxLook = -dxLook * sensitivity * 7.5;
        camera.look(dxLook, dyLook);
    }
}

void EngineWidget::wheelEvent(QWheelEvent *event) {
    double sensitivity = 0.001;

    double zoomDelta = event->delta();

    zoomDelta = zoomDelta * sensitivity;
    camera.zoom(zoomDelta);
}
