#include <QApplication>
#include <QVBoxLayout>

#include "Window.h"

int main(int argc, char *argv[]) {

    QApplication app(argc, argv);

    glutInit(&argc, argv);

    Window *window = new Window(NULL);

    window->resize(1024, 576);

    window->show();

    app.exec();

    delete window;

    return 0;
}
