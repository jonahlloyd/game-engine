#ifndef GAMEENGINE_ENGINEWIDGET_H
#define GAMEENGINE_ENGINEWIDGET_H

#include <QOpenGLWidget>
#include <QObject>
#include <GL/glut.h>
#include <QTime>
#include <QtGui>
#include <vector>
#include <QElapsedTimer>
#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QSpinBox>
#include <unistd.h>
#include <vector>
#include <tuple>

class EngineWidget : public QOpenGLWidget {
Q_OBJECT
public:
    EngineWidget(QWidget *parent);

    void CreateWorld();

    void DeclareObjects();

    void FrameRateGoverning();

    void CreateSpinBoxes();

protected:
    void initializeGL() override;

    void resizeGL(int w, int h) override;

    void paintGL() override;

    void mousePressEvent(QMouseEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;

    void wheelEvent(QWheelEvent *event) override;

    QPushButton *pause_button;
    QPushButton *stop_button;
    QPushButton *restart_button;
    QPushButton *reset_button;

    QPushButton *north_button;
    QPushButton *east_button;
    QPushButton *south_button;
    QPushButton *west_button;

    QDoubleSpinBox *xPos;
    QLabel *xPosLabel;
    QDoubleSpinBox *yPos;
    QLabel *yPosLabel;
    QDoubleSpinBox *zPos;
    QLabel *zPosLabel;
    QSpinBox *windSB;
    QLabel *windLabel;
    QSpinBox *velocity;
    QLabel *velocityLabel;

    QPoint lastPos;

public slots:

    void updateScene();

    void PlayPauseButton();

    void RestartButton();

    void StopButton();

    void ResetButton();

    void NorthButton();

    void EastButton();

    void SouthButton();

    void WestButton();

private:
    QTimer *timer;
    double delta, dTimePrev, dTimeCurr, dt;
    int paused, stopFlag;
    float ballX, ballY, ballZ, windSpeed, windDirX, windDirZ, uVelocity;
};


#endif
