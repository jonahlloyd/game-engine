#ifndef GAMEENGINE_PHYSICS_H
#define GAMEENGINE_PHYSICS_H

class Physics {
public:
    Physics();

    float KineticEnergy(float velocity, float mass);

    float gravity;
    float KE;
private:
    float gravity_p;

};


#endif
