#ifndef GAMEENGINE_WINDOW_H
#define GAMEENGINE_WINDOW_H

#include <QOpenGLWidget>
#include <QBoxLayout>
#include <QObject>
#include <GL/glut.h>
#include <QPushButton>

#include "EngineWidget.h"

namespace Ui {
    class Window;
}

class Window : public QWidget {

Q_OBJECT

public:

    // constructor / destructor
    explicit Window(QWidget *parent);

    ~Window();

    // window layout
    QBoxLayout *windowLayout;

    // beneath that, the main widget
    EngineWidget *engineWidget;

    void ResetInterface();
};

#endif
