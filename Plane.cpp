#include "Plane.h"
#include "Materials.h"

Materials materials_plane;

Plane::Plane() {}

float Plane::getX() {
    return width;
}

float Plane::getY() {
    return height;
}

float Plane::getZ() {
    return depth;
}

void Plane::create(int w, int d, int h) {
    width = w;
    depth = d;
    height = h;
    displayListId = glGenLists(1);
    glNewList(displayListId, GL_COMPILE);
    glBegin(GL_QUADS);
    glNormal3d(0, 1, 0);
    for (int x = 0; x < width - 1; x++) {
        for (int z = 0; z < depth - 1; z++) {
            // Draw triangles to produce alternating squares which creates a plane
            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE,
                    // Alternate between black and white squares
                         (x + z) % 2 == 0 ? materials_plane.BLACK : materials_plane.WHITE);
            glVertex3d(x, height, z);
            glVertex3d(x + 1, height, z);
            glVertex3d(x + 1, height, z + 1);
            glVertex3d(x, height, z + 1);
        }
    }
    glEnd();
    glEndList();
}

void Plane::draw() {
    glCallList(displayListId);
}
