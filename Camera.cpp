#include "Camera.h"

Camera::Camera() : y(8), x(5.5), z(15), theta(-90.0) {
    cameraFront = Vector(cos(theta * M_PI / 180), -0.5, sin(theta * M_PI / 180));
}

double Camera::getX() { return x; }

double Camera::getY() { return y; }

double Camera::getZ() { return z; }

void Camera::moveX(double dx) {
    x += dx;
}

void Camera::moveY(double dy) {
    y += dy;
}

void Camera::look(double thetaX, double thetaY) {
    // Look direction for y axes
    cameraFront.setY(thetaY + cameraFront.getY());
    // Look direction for x - z axis
    theta += thetaX;
    cameraFront.setX(cos(theta * M_PI / 180));
    cameraFront.setZ(sin(theta * M_PI / 180));
}

void Camera::zoom(double delta) {
    // Zoom in direction of camera's front
    x += cameraFront.getX() * delta;
    y += cameraFront.getY() * delta;
    z += cameraFront.getZ() * delta;
}

void Camera::reset() {
    x = 5.5;
    y = 12;
    z = 25;
    theta = -90.0;
    cameraFront = Vector(cos(theta*M_PI/180), -0.5, sin(theta*M_PI/180));
}
