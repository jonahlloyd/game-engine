#ifndef GAMEENGINE_BALL_H
#define GAMEENGINE_BALL_H

#include <GL/glut.h>
#include <ctime>
#include <chrono>
#include <iostream>
#include <tuple>
#include <cmath>
#include <QElapsedTimer>
#include <vector>

#include "Physics.h"
#include "Wind.h"
#include "Plane.h"

class Ball : public Physics {
public:
    Ball() {};

    Ball(Plane plane, float m, float r, GLfloat *c, float h, float x, float z, float uVelocity,
         Wind windObject, QElapsedTimer *GTimer, int *pausedFlag);

    void update();

    void draw();

    void Motion();

    void Velocity();

    void EnergyLoss();

    bool FloorCollision();

    void WindForce();

private:
    GLfloat *colour;
    float radius, x, y, z, height, mass, velocity, energyLoss, ballArea,
            dragCoefficient, windForce, acceleration, displacementXZ;
    int *pausedFlag;
    double time, dTime, timePrev;
    Plane plane;
    Wind wind;
    QElapsedTimer *GlobalTimer;
};


#endif
