#ifndef GAMEENGINE_WIND_H
#define GAMEENGINE_WIND_H

#include "Vector.h"

class Wind {
public:
    Wind() {}

    Wind(float directionX, float directionZ, float windVelocity);

    void North();

    void East();

    void South();

    void West();

    int DirX();

    int DirZ();

    float getPressure();

private:
    float windPressure, dirX, dirZ;
};


#endif
