#ifndef GAMEENGINE_VECTOR_H
#define GAMEENGINE_VECTOR_H

#include <cmath>
#include <iostream>

class Vector {
public:
    Vector() {}

    Vector(float x, float y, float z);

    float getX();

    float getY();

    float getZ();

    void setX(float dx);

    void setY(float dy);

    void setZ(float dz);

    Vector Add(Vector other);

    void Print();

private:
    float x, y, z;
};


#endif
