#include <iostream>

#include "Window.h"

Window::Window(QWidget *parent) : QWidget(parent) {

    // Create the window layout
    windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

    // Create main widget
    engineWidget = new EngineWidget(this);
    windowLayout->addWidget(engineWidget);
}

Window::~Window() {
    delete engineWidget;
    delete windowLayout;
}

// Resets all the interface elements
void Window::ResetInterface() {

    // Force refresh
    engineWidget->update();
    update();
}
