#include "Wind.h"

Wind::Wind(float directionX, float directionZ, float windVelocity):
            dirX(directionX), dirZ(directionZ){
    // Calculate the wind pressure from its velocity
    windPressure = 0.613 * windVelocity * windVelocity;
}

void Wind::North() {
  dirX = 0;
  dirZ = -1;
}

void Wind::East() {
  dirX = 1;
  dirZ = 0;
}

void Wind::South() {
  dirX = 0;
  dirZ = 1;
}

void Wind::West() {
  dirX = -1;
  dirZ = 0;
}

int Wind::DirX() {
  return dirX;
}

int Wind::DirZ() {
  return dirZ;
}


float Wind::getPressure() {
    return windPressure;
}
