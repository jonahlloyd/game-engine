#include "Vector.h"

Vector::Vector(float x, float y, float z) : x(x), y(y), z(z) {}

float Vector::getX() {
    return x;
}

float Vector::getY() {
    return y;
}

float Vector::getZ() {
    return z;
}

void Vector::setX(float dx) {
    x = dx;
}

void Vector::setY(float dy) {
    y = dy;
}

void Vector::setZ(float dz) {
    z = dz;
}

Vector Vector::Add(Vector other) {
    float i = x + other.getX();
    float j = y + other.getY();
    float k = z + other.getZ();
    return Vector(i, j, k);
}

void Vector::Print() {
    std::cout << x << " " << y << " " << z << '\n';
}
