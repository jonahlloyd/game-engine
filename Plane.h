#ifndef GAMEENGINE_PLANE_H
#define GAMEENGINE_PLANE_H

#include <GL/glut.h>

class Plane {
public:
    Plane();

    float getX();

    float getY();

    float getZ();

    void create(int width, int depth, int height);

    void draw();

private:
    int displayListId;
    int width;
    int depth;
    int height;
};


#endif
