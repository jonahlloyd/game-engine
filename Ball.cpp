#include "Ball.h"
#include "Physics.h"

Ball::Ball(Plane plane, float m, float r, GLfloat *c, float h, float x_axis, float z_axis, float uVelocity,
           Wind windObject, QElapsedTimer *GTimer, int *pausedFlag) :
        radius(r), colour(c), height(h), y(h), x(x_axis), z(z_axis), velocity(uVelocity), mass(m),
        energyLoss(0.9), wind(windObject), plane(plane), pausedFlag(pausedFlag) {

    GlobalTimer = GTimer;
    velocity = -velocity;
}

void Ball::update() {
    // Calculate time delta between current and previous frame
    dTime = GlobalTimer->nsecsElapsed() - timePrev;
    dTime = dTime * 1e-9;
    time = dTime;
    // Check motion isn't paused
    if (pausedFlag[0] == 0) {
        Velocity();
        Motion();
        WindForce();
    }
    timePrev = GlobalTimer->nsecsElapsed();
}

void Ball::draw() {
    glPushMatrix();
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colour);
    glTranslated(x, y, z);
    glutSolidSphere(radius, 30, 30);
    glPopMatrix();
}

void Ball::Motion() {
    y += 0.5 * velocity * time;
}

void Ball::Velocity() {
    // Calculate velocity using time delta
    velocity += gravity * time;
    if (FloorCollision()) {
        velocity = -velocity;
        EnergyLoss();
    }
}

void Ball::EnergyLoss() {
    //Calculate KE for ball when colliding with floor
    KE = KineticEnergy(velocity, mass);
    //Prevent ball continuously bouncing
    if (KE < -mass * gravity * 0.05) {
        velocity = velocity * 0.01;
    } else {
        velocity = (float) sqrt((double) ((2 * KE) / mass)) * energyLoss;
    }
}

bool Ball::FloorCollision() {
    return ((y - radius) <= plane.getY() &&
            (z <= (plane.getZ() - 1)) && (x <= (plane.getX() - 1)) &&
            (z >= 0) && (x >= 0));
}

void Ball::WindForce() {
    //Calculate necessary factors in wind force equation
    dragCoefficient = 0.47;
    ballArea = M_PI * radius * radius;
    windForce = ballArea * wind.getPressure() * dragCoefficient;
    acceleration = windForce / mass;
    //Calculate displacement using acceleration from wind
    displacementXZ = 0.5 * acceleration * time * time;
    // Direction of wind on x and z axis
    x += wind.DirX() * displacementXZ;
    z += wind.DirZ() * displacementXZ;
}
