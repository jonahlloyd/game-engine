#ifndef GAMEENGINE_CAMERA_H
#define GAMEENGINE_CAMERA_H

#include <cmath>
#include "Vector.h"

class Camera {
public:
    Camera();

    double getX();

    double getY();

    double getZ();

    void moveX(double dx);

    void moveY(double dy);

    void look(double thetaX, double thetaY);

    void zoom(double delta);

    void reset();

    Vector cameraFront;
private:
    double x;
    double y;
    double z;
    double theta;
};

#endif 
